# Changes in 2.1.1

## New features

- The `SSH_PUBKEY...` arguments now support multiline strings (for being able to include a
  single file with multiple pubkeys e.g. `"$(cat pubkeys*.pub)"`) and files as arguments (e.g.
  `/etc/ssh/ca.pub` without having to subshell out with `"$(cat ...)"`).

# Changes in 2.1.0

## New features

- The `-algos` option allows to specify a list of required host cert algorithms. If unset,
  accept any one host certificate(s). If the host offers no certificate, an error is raised.
- The optional `-krl` allows to specify a key revocation list to check if host certificates
  have been revoked

## Documentation

- Add icinga command and service example

# Changes in 2.0.0

## Backwards incompatible changes

- The `-auth` option has been removed. It is no longer required and does not change the result
  of the certificate validation.

## New features

- We are not requesting all known certificate types from a ssh server.
- Multiple CA pubkeys may be supplied, causing CA validation to succeed if the host key is
  signed by *any* one of the CAs.
- We are now checking that principals of the certificates match. By default, the principals
  need to contain at least the supplied `HOST`. This can be overwritten by the `-principal`
  option.

## Bugfixes

- The logic for expiration checking was fixed.

# Changes in 1.0.0

Initial release
