package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/NETWAYS/go-check"
	"github.com/NETWAYS/go-check/result"
	k "github.com/porkbeans/krl"
	"golang.org/x/crypto/ssh"
)

// someone asked and implemented a similar checker
// https://serverfault.com/questions/619979/ssh-host-key-certificates-find-validity-period-remotely/622022

// someone implemented a pretty similar thing on github
// https://github.com/Eun/sshkeys

type Config struct {
	netproto             string
	host                 string
	port                 uint
	requiredPrincipals   []string
	timeout              time.Duration
	requiredHostKeyAlgos map[string]bool
}

func hostKeyCallback(certp *ssh.Certificate) func(hostname string, remote net.Addr, key ssh.PublicKey) error {
	return func(hostname string, remote net.Addr, key ssh.PublicKey) error {
		if cert, ok := key.(*ssh.Certificate); ok {
			*certp = *cert
		}
		return nil
	}
}

func getCert(algo string, c *Config) (cert ssh.Certificate, err error) {
	d := net.Dialer{Timeout: c.timeout}
	target := fmt.Sprintf("[%s]:%d", c.host, c.port)
	conn, err := d.Dial(c.netproto, target)
	if err != nil {
		// return nil, err is not possible because we need to return an actual (in
		// this case empty) cert
		return ssh.Certificate{}, err
	}
	defer conn.Close()

	config := ssh.ClientConfig{
		User:              "check_ssh_host_cert",
		HostKeyAlgorithms: []string{algo},
		HostKeyCallback:   hostKeyCallback(&cert),
		Timeout:           c.timeout,
	}
	sshconn, _, _, err := ssh.NewClientConn(conn, target, &config)
	if err == nil {
		sshconn.Close()
	}
	return cert, nil
}

func getHostCerts(c *Config) ([]ssh.Certificate, error) {
	type result struct {
		cert *ssh.Certificate
		err  error
	}

	supportedHostKeyAlgos := make([]string, 0, len(c.requiredHostKeyAlgos))
	for k := range c.requiredHostKeyAlgos {
		supportedHostKeyAlgos = append(supportedHostKeyAlgos, k)
	}

	var wg sync.WaitGroup
	results := make(chan result, len(supportedHostKeyAlgos))
	for _, algo := range supportedHostKeyAlgos {
		wg.Add(1)
		go func(algo string) {
			defer wg.Done()
			cert, err := getCert(algo, c)
			results <- result{&cert, err}
		}(algo)
	}
	wg.Wait()
	certs := make([]ssh.Certificate, len(supportedHostKeyAlgos))
	for range supportedHostKeyAlgos {
		result := <-results
		if result.err != nil {
			return nil, result.err
		}
		if result.cert != nil {
			certs = append(certs, *result.cert)
		}
	}
	return certs, nil
}

func usage() {
	fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s:\n", os.Args[0])
	flag.PrintDefaults()
	fmt.Fprintf(flag.CommandLine.Output(), "  HOST\n\tHost to be checked for certs. Example: git.example.org\n")
	fmt.Fprintf(flag.CommandLine.Output(), "  SSH_PUBKEY...\n\tOne or more SSH public key strings or paths to files of the signing CA(s). If multiple pubkeys are given, the certificate must be signed by any one of then. Example: \"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM6aV1Xe/ZxQ87dbr1CKNALaU41bt+DjsqpdbyJJFoIg\" or /etc/ssh/ca.pub.\n")
	os.Exit(3)
}

func main() {
	defer check.CatchPanic()

	c := &Config{}
	// from ssh.supportedHostKeyAlgos
	// XXX it's not exported so we need to track openssh releases and extend this list
	// with all new ssh certificate host key algorithms except sk- (fido) versions
	c.requiredHostKeyAlgos = map[string]bool{
		ssh.CertAlgoRSAv01:      false,
		ssh.CertAlgoDSAv01:      false,
		ssh.CertAlgoECDSA256v01: false,
		ssh.CertAlgoECDSA384v01: false,
		ssh.CertAlgoECDSA521v01: false,
		ssh.CertAlgoED25519v01:  false,
	}
	v4 := flag.Bool("4", false, "Connect only via legacy IP (IPv4). Optional.")
	v6 := flag.Bool("6", false, "Connect only via IPv6. Optional.")
	flag.UintVar(&c.port, "port", 22, "ssh port")
	warn := flag.Uint("warning", 30, "expiration warning in days")
	crit := flag.Uint("critical", 14, "expiration critical in days")
	principals := flag.String("principals", "", "List all required certificate principals. Default value: the HOST supplied. If set, replaces the HOST and only uses the supplied values.")
	algos := flag.String("algos", "", "Give a list of comma separated required host key algorithms. Default: none required and all accepted. For a comprehensive list, please consult the ssh manual or manpage (ssh -Q HostbasedKeyTypes | grep -- -cert). Example: ssh-ed25519-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com")
	krlarg := flag.String("krl", "", "Provide the path to a key revocation list (krl). Default: none.")
	flag.Usage = usage

	flag.Parse()
	args := flag.Args()

	if len(args) < 2 {
		flag.Usage()
	}

	if c.port >= 1<<16 {
		flag.Usage()
	}
	if *v4 && *v6 {
		flag.Usage()
	}

	var warning, critical time.Duration
	warning = time.Duration(*warn*24) * time.Hour
	critical = time.Duration(*crit*24) * time.Hour

	c.host = args[0]

	if *principals == "" {
		c.requiredPrincipals = []string{c.host}
	} else {
		c.requiredPrincipals = strings.Split(*principals, ",")
	}

	if *algos != "" {
		for _, algo := range strings.Split(*algos, ",") {
			if _, ok := c.requiredHostKeyAlgos[algo]; ok {
				c.requiredHostKeyAlgos[algo] = true
			} else {
				check.Exit(3, fmt.Sprintf("unknown host key algorithm %s", algo))
			}
		}
	}

	var krl *k.KRL
	if *krlarg != "" {
		raw, err := ioutil.ReadFile(*krlarg)
		if err != nil {
			check.Exit(3, fmt.Sprintf("Could not read KRL: %s", err.Error()))
		}
		krl, err = k.ParseKRL(raw)
		if err != nil {
			check.Exit(3, fmt.Sprintf("Could not parse KRL: %s", err.Error()))
		}
	}

	c.timeout = 3 * time.Second

	overall := result.Overall{}
	pubkeys := make([]ssh.PublicKey, 0, len(args[1:]))

	// accept multiple SSH_PUBKEY... but also accept any of those to include newlines
	// to be able to "$(cat /etc/ssh/ca*.pub)" as argument
	// any include any lines if the argument points to a file
	for _, pubkeyarg := range args[1:] {
		// this pubkey might actually be a file
		if _, err := os.Stat(pubkeyarg); !os.IsNotExist(err) {
			raw, err := ioutil.ReadFile(pubkeyarg)
			if err != nil {
				check.Exit(3, fmt.Sprintf("Could not read file %s", pubkeyarg))
			}
			pubkeyarg = string(raw)
		}
		for _, stringkey := range strings.Split(pubkeyarg, "\n") {
			// files ending in \n produce an empty string as last iteration
			if stringkey == "" {
				continue
			}
			pubkey, _, _, _, err := ssh.ParseAuthorizedKey([]byte(stringkey))
			if err != nil {
				check.Exit(3, fmt.Sprintf("unable to parse pubkey: %s", err.Error()))
			}
			pubkeys = append(pubkeys, pubkey)
		}
	}

	c.netproto = "tcp"
	if *v4 {
		c.netproto = "tcp4"
	}
	if *v6 {
		c.netproto = "tcp6"
	}
	certs, err := getHostCerts(c)
	if err != nil {
		check.Exit(3, fmt.Sprintf("Could not retrieve host certificate(s): %s", err.Error()))
	}

	// required host key check
	for algo, required := range c.requiredHostKeyAlgos {
		if !required {
			continue
		}
		found := false
		for _, cert := range certs {
			if cert.Key != nil && cert.Type() == algo {
				found = true
			}
		}
		if !found {
			overall.AddCritical(fmt.Sprintf("Host certificate %s was not found albeit required", algo))
		}
	}

	numCerts := 0
	for _, cert := range certs {
		if cert.SignatureKey == nil {
			continue
		}
		numCerts++
		foundSigner := false

		// signing check
		for _, pubkey := range pubkeys {
			if bytes.Equal(cert.SignatureKey.Marshal(), pubkey.Marshal()) {
				foundSigner = true
				break
			}
		}
		if !foundSigner {
			overall.AddCritical(fmt.Sprintf("Host certificate %s is not signed by any of the supplied CA", cert.Type()))
		} else {
			overall.AddOK(fmt.Sprintf("Host certificate %s is signed by any of the supplied CA", cert.Type()))
		}

		// principal checks
		principalMap := make(map[string]bool, len(c.requiredPrincipals))
		for _, principal := range c.requiredPrincipals {
			principalMap[principal] = false
		}
		for _, principal := range cert.ValidPrincipals {
			principalMap[principal] = true
		}
		for _, principal := range c.requiredPrincipals {
			if !principalMap[principal] {
				overall.AddCritical(fmt.Sprintf("Host certificate %s does not list host %s in principals", cert.Type(), principal))
			} else {
				overall.AddOK(fmt.Sprintf("Host certificate %s lists host %s in principals", cert.Type(), principal))
			}
		}

		// expiration checks
		// a certificate validity forever is modelled as expiring -1
		expiring := time.Unix(int64(cert.ValidBefore), 0)
		if expiring.Unix() == -1 {
			overall.AddOK(fmt.Sprintf("Host certificate %s is never going to expire", cert.Type()))
		} else if expiring.Before(time.Now()) {
			overall.AddCritical(fmt.Sprintf("Host certificate %s has expired at %v", cert.Type(), expiring))
		} else if time.Now().Add(critical).After(expiring) {
			overall.AddCritical(fmt.Sprintf("Host certificate %s is expiring at %v", cert.Type(), expiring))
		} else if time.Now().Add(warning).After(expiring) {
			overall.AddWarning(fmt.Sprintf("Host certificate %s is expiring at %v", cert.Type(), expiring))
		} else {
			overall.AddOK(fmt.Sprintf("Host certificate %s is expiring at %v", cert.Type(), expiring))
		}

		// revocation check
		if krl != nil {
			if krl.IsRevoked(&cert) {
				overall.AddCritical(fmt.Sprintf("Host certificate %s is revoked by KRL", cert.Type()))
			} else {
				overall.AddOK(fmt.Sprintf("Host certificate %s not is revoked by KRL", cert.Type()))
			}

		}
	}
	if numCerts == 0 {
		overall.AddCritical("No host certificate(s) offered by the server")
	}

	check.Exit(overall.GetStatus(), overall.GetOutput())
}
