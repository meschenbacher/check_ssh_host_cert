# `check_ssh_host_cert`

`check_ssh_host_cert` is a tool (created as icinga2 check plugin) which can be
used to verify that the ssh server of a host offers ssh certificates signed by one of
(possible multiple) ssh ca key(s).

```
Usage of ./check_ssh_host_cert:
  -4	Connect only via legacy IP (IPv4). Optional.
  -6	Connect only via IPv6. Optional.
  -algos string
    	Give a list of comma separated required host key algorithms. Default: none required and all accepted. For a comprehensive list, please consult the ssh manual or manpage (ssh -Q HostbasedKeyTypes | grep -- -cert). Example: ssh-ed25519-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com
  -critical uint
    	expiration critical in days (default 14)
  -krl string
    	Provide the path to a key revocation list (krl). Default: none.
  -port uint
    	ssh port (default 22)
  -principals string
    	List all required certificate principals. Default value: the HOST supplied. If set, replaces the HOST and only uses the supplied values.
  -warning uint
    	expiration warning in days (default 30)
  HOST
	Host to be checked for certs. Example: git.example.org
  SSH_PUBKEY...
	One or more SSH public key strings or paths to files of the signing CA(s). If multiple pubkeys are given, the certificate must be signed by any one of then. Example: "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM6aV1Xe/ZxQ87dbr1CKNALaU41bt+DjsqpdbyJJFoIg" or /etc/ssh/ca.pub.
```

example output
```
$ ./check_ssh_host_cert localhost "$(cat ca1.pub)"
OK - states: ok=6
[OK] Host certificate ssh-ed25519-cert-v01@openssh.com is signed by any of the supplied CA
[OK] Host certificate ssh-ed25519-cert-v01@openssh.com lists host localhost in principals
[OK] Host certificate ssh-ed25519-cert-v01@openssh.com is expiring at 2021-06-11 11:02:22 +0200 CEST
[OK] Host certificate ssh-rsa-cert-v01@openssh.com is signed by any of the supplied CA
[OK] Host certificate ssh-rsa-cert-v01@openssh.com lists host localhost in principals
[OK] Host certificate ssh-rsa-cert-v01@openssh.com is expiring at 2021-06-11 11:02:22 +0200 CEST

$ ./check_ssh_host_cert -krl revoked_keys -algos ssh-ed25519-cert-v01@openssh -principals git,git.example.org,git.example.com git.example.org "$(cat ca1.pub)"
OK - states: ok=6
[OK] Host certificate ssh-ed25519-cert-01@openssh.com is signed by any of the supplied CA
[OK] Host certificate ssh-ed25519-cert-v01@openssh.com lists host git in principals
[OK] Host certificate ssh-ed25519-cert-v01@openssh.com lists host git.example.org in principals
[OK] Host certificate ssh-ed25519-cert-v01@openssh.com lists host git.example.com in principals
[OK] Host certificate ssh-ed25519-cert-v01@openssh.com is never going to expire
[OK] Host certificate ssh-ed25519-cert-v01@openssh.com not is revoked by KRL

$ ./check_ssh_host_cert localhost "$(cat ca1.pub)"
CRITICAL - states: critical=4 ok=2
[CRITICAL] Host certificate ssh-rsa-cert-v01@openssh.com is not signed by any of the supplied CA
[OK] Host certificate ssh-rsa-cert-v01@openssh.com lists host localhost in principals
[CRITICAL] Host certificate ssh-rsa-cert-v01@openssh.com has expired at 2021-04-12 01:45:07 +0200 CEST
[CRITICAL] Host certificate ssh-ed25519-cert-v01@openssh.com is not signed by any of the supplied CA
[OK] Host certificate ssh-ed25519-cert-v01@openssh.com lists host localhost in principals
[CRITICAL] Host certificate ssh-ed25519-cert-v01@openssh.com has expired at 2021-04-12 01:45:06 +0200 CEST
```

# build

```
go build
```

# Integrate into icinga

Command template

```
object CheckCommand "ssh host cert" {
	import "plugin-check-command"
	command = [ "/usr/local/bin/check_ssh_host_cert" ]
	arguments = {
		"-4" = {
			set_if = "$sshhostcert_ipv4$"
		}
		"-6" = {
			set_if = "$sshhostcert_ipv6$"
		}
		"-algos" = {
			value = "$sshhostcert_algos$"
			set_if = "$sshhostcert_algos$"
		}
		"-port" = {
			value = "$sshhostcert_port$"
			set_if = "$sshhostcert_port$"
		}
		"-critical" = {
			value = "$sshhostcert_critical$"
			set_if = "$sshhostcert_critical$"
		}
		"-warning" = {
			value = "$sshhostcert_warning$"
			set_if = "$sshhostcert_warning$"
		}
		"-principals" = {
			value = "$sshhostcert_principals$"
			set_if = "$sshhostcert_principals$"
		}
		"-krl" = {
			value = "$sshhostcert_krl$"
			set_if = "$sshhostcert_krl$"
		}
		"host" = {
			value = "$sshhostcert_host$"
			skip_key = true
		}
		"pubkeys" = {
			value = "$sshhostcert_cas$"
			skip_key = true
			repeat_key = false
		}
	}
}
```

Service example

```
apply Service "ssh host cert" {
	import "generic-service"
	check_command = "ssh host cert"
	vars.sshhostcert_host = host.name
	vars.sshhostcert_cas = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM6aV1Xe/ZxQ87dbr1CKNALaU41bt+DjsqpdbyJJFoIg" ]
	command_endpoint = "icinga-master.example.org"
	assign where "linux-servers" in host.groups
}
```
