module gitlab.com/meschenbacher/check_ssh_host_cert

go 1.15

require (
	github.com/NETWAYS/go-check v0.0.0-20201207101832-89c3bfada341
	github.com/porkbeans/krl v0.0.0-20190120140803-7569b7c3179a
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
	golang.org/x/sys v0.0.0-20210301091718-77cc2087c03b // indirect
)
